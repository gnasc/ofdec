# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.box = "freebsd/FreeBSD-10.3-RELEASE"

  config.vm.define "Database" do |db|
      db.vm.hostname = "Database"
      db.vm.base_mac = "0800278B0101"
      db.ssh.shell = "/bin/sh"
      db.vm.synced_folder ".",
          "/vagrant",
          type: "rsync"

      db.vm.synced_folder "salt",
          "/usr/local/etc/salt",
          type: "rsync"

      db.vm.provision :salt do |salt|
          salt.run_highstate = true
          salt.masterless = true
          salt.minion_config = "salt/minion"

          salt.verbose = true
          salt.log_level = "info"

          salt.bootstrap_script = "salt/bootstrap.sh"
          salt.bootstrap_options = "-P"
      end

      db.vm.network "forwarded_port",
          guest: 3306,
          host: 13306

      db.vm.network "private_network",
          ip: "10.101.0.101",
          netmask: "255.255.255.0"
  end

  config.vm.define "Webapp" do |web|
      web.vm.hostname = "Webapp"
      web.vm.base_mac = "0800278B0110"
      web.ssh.shell = "/bin/sh"
      web.vm.synced_folder ".",
          "/vagrant",
          type: "rsync"

      web.vm.synced_folder "salt",
          "/usr/local/etc/salt",
          type: "rsync"

      web.vm.provision :salt do |salt|
          salt.run_highstate = true
          salt.masterless = true
          salt.minion_config = "salt/minion"

          salt.verbose = true
          salt.log_level = "info"

          salt.bootstrap_script = "salt/bootstrap.sh"
          salt.bootstrap_options = "-P"
      end

      web.vm.network "forwarded_port",
          guest: 80,
          host: 8080

      web.vm.network "private_network",
          ip: "10.101.0.102",
          netmask: "255.255.255.0"
  end

  config.vm.define "Monitoring" do |monit|
      monit.vm.hostname = "Monitoring"
      monit.vm.base_mac = "0800278B0111"
      monit.ssh.shell = "/bin/sh"

      monit.vm.synced_folder ".",
          "/vagrant",
          type: "rsync"

      monit.vm.synced_folder "salt",
          "/usr/local/etc/salt",
          type: "rsync"

      monit.vm.provision :salt do |salt|
          salt.run_highstate = true
          salt.masterless = true
          salt.minion_config = "salt/minion"

          salt.verbose = true
          salt.log_level = "info"

          salt.bootstrap_script = "salt/bootstrap.sh"
          salt.bootstrap_options = "-P"
      end

      monit.vm.network "forwarded_port",
          guest: 3000,
          host: 3000

      monit.vm.network "private_network",
          ip: "10.101.0.103",
          netmask: "255.255.255.0"
  end
end
