
ofdec
=====

Creates a virtual machine called "Database" running FreeBSD. You need to have vagrant installed to start the VM.

**Disclaimer:** The following instructions were run on a machine running Ubuntu 16.10 amd64.

To install Vagrant and Vagrant's SaltStack plugin, run:

```
$ sudo apt install vagrant
$ vagrant plugin install vagrant-salt
```

To start the VM, run:

```
$ vagrant up
```

In the first run, Vagrant may need to download the box and FreeBSD may download some patches, so it is okay if it takes some time to start. Also it is okay if Vagrant can't establish a SSH connection the first time the VM runs. After the VM restarts to apply the patches, everything should work fine though.

