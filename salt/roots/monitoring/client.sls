# vim:ft=yaml ts=2

sensu-client:
  file.managed:
    - name: /usr/local/etc/sensu/conf.d/client.json
    - source: salt://monitoring/client.json
  service.running:
    - name: sensu-client
    - restart: True

rabbitmq-client:
  file.managed:
    - name: /usr/local/etc/sensu/conf.d/rabbitmq.json
    - source: salt://monitoring/rabbitmq.json
    - template: jinja
    - context:
        host: "10.101.0.103"

