# vim:ft=yaml ts=2

server-deps:
  pkg.installed:
    - pkgs:
      - rabbitmq
      - redis
      - uchiwa
    - reload_modules: True

sensu-client:
  file.managed:
    - name: /usr/local/etc/sensu/conf.d/client.json
    - source: salt://monitoring/client.json
  service.running:
    - name: sensu-client
    - restart: True

rabbitmq-server:
  file.managed:
    - name: /usr/local/etc/rabbitmq/rabbitmq.config
    - source: salt://monitoring/rabbitmq.config

rabbitmq-client:
  file.managed:
    - name: /usr/local/etc/sensu/conf.d/rabbitmq.json
    - source: salt://monitoring/rabbitmq.json
    - template: jinja
    - context:
        host: "localhost"

rabbitmq-service:
  service.running:
    - name: rabbitmq
    - restart: True
    - require:
      - pkg: server-deps

rabbitmq-vhost:
  cmd.run:
    - name: rabbitmqctl add_vhost /sensu || echo "exists"
    - require:
      - service: rabbitmq-service

rabbitmq-user:
  cmd.run:
    - name: rabbitmqctl add_user sensu sensu || echo "exists"
    - require:
      - service: rabbitmq-service

rabbitmq-permissions:
  cmd.run:
    - name: rabbitmqctl set_permissions -p /sensu sensu ".*" ".*" ".*" || echo "exists"
    - require:
      - service: rabbitmq-service

redis:
  file.managed:
    - name: /usr/local/etc/sensu/conf.d/redis.json
    - source: salt://monitoring/redis.json
  service.running:
    - name: redis
    - restart: True

sensu-api:
  service.running:
    - name: sensu-api
    - restart: True

sensu-server:
  service.running:
    - name: sensu-server
    - restart: True

uchiwa:
  file.managed:
    - name: /usr/local/etc/uchiwa/config.json
    - source: salt://monitoring/uchiwa.json
  service.running:
    - name: uchiwa
    - restart: True

