# vim:ft=yaml ts=2

sensu-deps:
  pkg.installed:
    - pkgs:
      - curl

sensu-download:
  cmd.run:
    - name: curl -Lo - https://sensu.global.ssl.fastly.net/freebsd/FreeBSD:10:amd64/sensu/sensu-0.29.0_11.txz | pkg add -
    - creates: /usr/local/etc/sensu
    - require:
      - pkg: sensu-deps

