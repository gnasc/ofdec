# vim:ft=yaml ts=2

base:
  'Database':
    - mysql/mysql
    - monitoring/sensu
    - monitoring/client
  'Webapp':
    - webapp/webapp
    - monitoring/sensu
    - monitoring/client
  'Monitoring':
    - monitoring/sensu
    - monitoring/server
