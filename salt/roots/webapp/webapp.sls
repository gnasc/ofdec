# vim:ft=yaml ts=2

deps:
  pkg.installed:
    - pkgs:
      - git
      - wget
      - mysql56-client

go-install:
  archive.extracted:
    - name: /usr/local
    - source: https://storage.googleapis.com/golang/go1.8.1.freebsd-amd64.tar.gz
    - source_hash: 91065d4bb311bba74d7c1e3e6bab82fbba24c9da8aeca9e803caabcc66de1af7
    - archive_format: tar
    - if_missing: /usr/local/go

go-bin:
  file.symlink:
    - name: /usr/local/bin/go
    - target: /usr/local/go/bin/go

go-home:
  file.directory:
    - name: /home/vagrant/go
    - user: vagrant
    - group: vagrant
    - dir_mode: 755
    - file_mode: 644

gowebapp-get:
  cmd.run:
    - name: |
        go get github.com/josephspurrier/gowebapp |& cat
    - runas: vagrant
    - cwd: /home/vagrant/go
    - creates: /home/vagrant/go/src/github.com/josephspurrier/gowebapp

gowebapp-mysql:
  file.managed:
    - name: /home/vagrant/go/src/github.com/josephspurrier/gowebapp/config/config.json
    - source: salt://webapp/config.json
    - user: vagrant
    - group: vagrant
    - mode: 644
    - require:
      - file: go-home

# This password on the CLI is a security issue. I need to fix it
# Check whether there's a better way to import the DB
gowebapp-importdb:
  cmd.run:
    - name: |
        mysql -h10.101.0.101 -p3306 -uwebapp -pwebapp gowebapp < /home/vagrant/go/src/github.com/josephspurrier/gowebapp/config/mysql.sql && touch /home/vagrant/imported
    - require:
      - pkg: deps

gowebapp-build:
  cmd.run:
    - name: "go build"
    - cwd: /home/vagrant/go/src/github.com/josephspurrier/gowebapp/
    - runas: vagrant

gowebapp-rcfile:
  file.managed:
    - name: /usr/local/etc/rc.d/gowebapp
    - source: salt://webapp/gowebapp
    - user: root
    - group: wheel
    - mode: 755

gowebapp-enable:
  file.append:
    - name: /etc/rc.conf
    - text: "gowebapp_enable=\"YES\""

