# vim:ft=yaml ts=2

mysql56:
  pkg.installed:
    - pkgs:
      - mysql56-server
      - mysql56-client
      - python
      - py27-pip

mysql-python:
  pip.installed:
    - name: MySQL-python
    - require:
      - pkg: mysql56

mysql-server:
  service.running:
    - name: mysql-server
    - require:
      - pkg: mysql56
  file.managed:
    - name: /usr/local/etc/mysql/my.cnf
    - source: salt://mysql/my.cnf
    - user: root
    - group: wheel
    - mode: 644
    - require:
      - pkg: mysql56

webapp-database:
  mysql_database.present:
    - name: gowebapp
    - require:
      - service: mysql-server
      - pip: mysql-python

webapp-user:
  mysql_user.present:
    - name: webapp
    - host: '%'
    - password: 'webapp'
    - connection_charset: 'utf8'
    - require:
      - service: mysql-server
      - pip: mysql-python

webapp-grants:
  mysql_grants.present:
    - grant: all privileges
    - database: gowebapp.*
    - user: webapp
    - host: '%'
    - require:
      - mysql_user: webapp-user

